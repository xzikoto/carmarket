﻿using System.Text.Json.Serialization;
using CarMarket.Components;

namespace CarMarket.Shared.Models
{
    public class CategoryModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("iconClass")]
        public string IconClass { get; set; }

        [JsonPropertyName("createdOnUtc")]
        public DateTime CreatedOnUtc { get; set; }

        [JsonPropertyName("modifiedOnUtc")]
        public DateTime? ModifiedOnUtc { get; set; }
    }
}
