﻿using System.Text.Json.Serialization;

namespace CarMarket.Shared.Models
{
    public class ItemModel
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }


        [JsonPropertyName("name")]
        public string Name { get; set; }


        [JsonPropertyName("categoryId")]
        public int CategoryId { get; set; }


        [JsonPropertyName("batteryHealth")]
        public int BatteryHealth { get; set; }


        [JsonPropertyName("base64Image")]
        public string Base64Image { get; set; }
    }
}
