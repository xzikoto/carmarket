﻿using CarMarket.Infrastructure.Services.AwsServices;
using Microsoft.Extensions.Options;

namespace carmarket_web_app.OptionsSetup
{
    public class AwsOptionsSetup : IConfigureOptions<AwsOptions>
    {
        private const string SectionName = "AWS";
        private readonly IConfiguration _configuration;

        public AwsOptionsSetup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Configure(AwsOptions options)
        {
            _configuration.GetSection(SectionName).Bind(options);
        }
    }
}
