﻿using System.Text.Json;
using CarMarket.Client.ApiServices.Core;
using CarMarket.Shared.Models;

namespace CarMarket.Client.ApiServices
{
    public class ItemApiService : IItemApiService
    {
        private readonly HttpClient _httpClient;
        public ItemApiService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<ItemModel>> GetItemsByCategoryId(int categoryId, CancellationToken cancellationToken = default)
        {
            var response = await _httpClient.GetAsync($"items/{categoryId}", cancellationToken);
            
            if (response.Content == null)
            {
                return new List<ItemModel>();
            }
            
            var result = JsonSerializer.Deserialize<List<ItemModel>>(await response.Content.ReadAsStringAsync(cancellationToken));

            return result;
        }
    }
}
