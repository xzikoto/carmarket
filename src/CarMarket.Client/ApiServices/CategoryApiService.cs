﻿using CarMarket.Shared.Models;
using System.Text.Json;
using CarMarket.Client.ApiServices.Core;

namespace CarMarket.Client.ApiServices
{

    public class CategoryApiService : ICategoryApiService
    {
        private readonly HttpClient _httpClient;

        public CategoryApiService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<CategoryModel>> GetCategoryLocalizations(int languageId, CancellationToken cancellationToken)
        {
            var response = await _httpClient.GetAsync($"categories/{languageId}", cancellationToken);
            if (response.Content == null)
            {
                return new List<CategoryModel>();
            }

            var result = JsonSerializer.Deserialize<List<CategoryModel>>(await response.Content.ReadAsStringAsync(cancellationToken));

            return result;
        }
    }
}
