﻿using CarMarket.Shared.Models;

namespace CarMarket.Client.ApiServices.Core
{
    public interface IItemApiService
    {
        public Task<List<ItemModel>> GetItemsByCategoryId(int categoryId, CancellationToken cancellationToken = default);
    }
}
