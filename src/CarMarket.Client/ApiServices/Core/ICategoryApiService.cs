﻿
using CarMarket.Shared.Models;

namespace CarMarket.Client.ApiServices.Core
{
    public interface ICategoryApiService
    {
        Task<List<CategoryModel>> GetCategoryLocalizations(int languageId, CancellationToken cancellationToken = default);
    }
}
