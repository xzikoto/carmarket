﻿using CarMarket.Client.ApiServices.Core;
using CarMarket.Shared.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace CarMarket.Client.Pages.Categories
{
    public partial class CategoriesBase : ComponentBase
    {
        [Inject]
        private ICategoryApiService CategoryService { get; set;}

        protected List<CategoryModel>? categories;

        protected int SelectedCategoryId { get; set; }
        protected bool showItems = true;

        protected override async Task OnInitializedAsync()
        {
            await GetCategories();
            GetDefaultCategoryId();
        }

        public void GetDefaultCategoryId()
        {
            if (categories != null && categories.Any())
            {
                SelectedCategoryId = categories.First().Id;
            }
        }

        public async Task GetCategories()
        {
            categories = await CategoryService.GetCategoryLocalizations(1, new CancellationToken());
        }

        public void CategoryClick(MouseEventArgs e, int categoryId)
        {
            SelectedCategoryId = categoryId;
        }
    }
}
