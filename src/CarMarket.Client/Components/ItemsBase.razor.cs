﻿using CarMarket.Client.ApiServices.Core;
using CarMarket.Shared.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace CarMarket.Client.Components
{
    public class ItemsBase : ComponentBase
    {
        [Inject]
        private IItemApiService ItemApiService { get; set; }

        [Inject]
        private IJSRuntime JsRuntime { get; set; }

        protected List<ItemModel>? items { get; set; }

        [Parameter]
        public int LocationId { get; set; }

        protected override async Task OnParametersSetAsync()
        {
            await GetItemsByCategoryId(LocationId);

            await base.OnParametersSetAsync();
        }

        protected override async Task OnInitializedAsync()
        {
            await GetItemsByCategoryId(LocationId);
        }

        public async Task GetItemsByCategoryId(int categoryId, CancellationToken cancellationToken = default)
        {
            items = await ItemApiService.GetItemsByCategoryId(categoryId, cancellationToken);
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await JsRuntime.InvokeVoidAsync("initJQueryElements");
            }
        }
    }
}
