using CarMarket.Client.ApiServices;
using CarMarket.Client.ApiServices.Core;

namespace CarMarket.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddRazorPages();
            builder.Services.AddServerSideBlazor();


            builder.Services.AddTransient<ICategoryApiService, CategoryApiService>();
            builder.Services.AddHttpClient<ICategoryApiService, CategoryApiService>(client =>
            {
                var DefaultApi = builder.Configuration.GetValue<string>("ApiUrl:BaseAddress");
                client.BaseAddress = new Uri(DefaultApi);

            }); 

            builder.Services.AddTransient<IItemApiService, ItemApiService>();
            builder.Services.AddHttpClient<IItemApiService, ItemApiService>(client =>
            {
                var DefaultApi = builder.Configuration.GetValue<string>("ApiUrl:BaseAddress");
                client.BaseAddress = new Uri(DefaultApi);
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.MapBlazorHub();
            app.MapFallbackToPage("/_Host");

            app.Run();
        }
    }
}