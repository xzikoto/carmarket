﻿using CarMarket.Application.Abstractions.Messaging;

namespace CarMarket.Application.Members.GetMemberByIdQuery;

public sealed record GetMemberByIdQuery(Guid MemberId) : IQuery<MemberResponse>;