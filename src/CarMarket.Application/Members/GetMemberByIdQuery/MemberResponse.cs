﻿namespace CarMarket.Application.Members.GetMemberByIdQuery;

public sealed record MemberResponse(Guid Id, string Email);