﻿namespace CarMarket.Application.Members.Login;

public record LoginCommand(string Email): ICommand<string>;