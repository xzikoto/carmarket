﻿namespace CarMarket.Application.Members.Login;

public record LoginRequest(string Email);