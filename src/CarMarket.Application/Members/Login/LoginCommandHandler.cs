﻿using CarMarket.Application.Abstractions;
using CarMarket.Application.Abstractions.Messaging;
using CarMarket.Domain.Entities;
using CarMarket.Domain.Errors;
using CarMarket.Domain.Repositories;
using CarMarket.Domain.Shared;
using CarMarket.Domain.ValueObjects;

namespace CarMarket.Application.Members.Login;

internal sealed class LoginCommandHandler : ICommandHandler<LoginCommand, string>
{
    private readonly IMemberRepository _memberRepository;
    private readonly IJwtProvider _jwtProvider;

    public LoginCommandHandler(
        IMemberRepository memberRepository,
        IJwtProvider jwtProvider)
    {
        _memberRepository = memberRepository;
        _jwtProvider = jwtProvider;
    }

    public async Task<Result<string>> Handle(LoginCommand request, CancellationToken cancellationToken)
    {
        Result<Email> email = Email.Create(request.Email);

        Member? member = await _memberRepository.GetByEmailAsync(
            email.Value,
            cancellationToken);

        if (member is null)
        {
            return Result.Failure<string>(
                DomainErrors.Member.InvalidCredentials);
        }

        string token = _jwtProvider.Generate(member);

        return token;
    }
}

