﻿using AutoMapper;
using CarMarket.Application.Items.GetAllByCategory;
using CarMarket.Domain.Entities.Items;

namespace CarMarket.Application.Mappings;

public class ItemsDomainToDtoMapper : Profile
{
    public ItemsDomainToDtoMapper()
    {
        CreateMap<Item, ItemDto>();
    }
}