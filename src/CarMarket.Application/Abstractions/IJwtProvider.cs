﻿using CarMarket.Domain.Entities;

namespace CarMarket.Application.Abstractions;

public interface IJwtProvider
{
    string Generate(Member member);
}