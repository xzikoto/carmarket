﻿using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;

namespace CarMarket.Application.Abstractions;

public interface IItemImagesService
{
    Task<PutObjectResponse> UploadImageAsync(Guid itemId, IFormFile file, bool isMain);

    Task<GetObjectResponse?> GetImageAsync(Guid itemId, string imageName);

    Task<List<GetObjectResponse>?> GetAllItemImagesAsync(Guid itemId, int itemsPerPage);

    Task<List<GetObjectResponse>?> GetAllMainByCategoryIdAsync(int categoryId, int itemsPerPage);

    Task<DeleteObjectResponse> DeleteImageAsync(Guid itemId, string imageName);
}
