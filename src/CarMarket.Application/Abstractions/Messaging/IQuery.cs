﻿using CarMarket.Domain.Shared;
using MediatR;

namespace CarMarket.Application.Abstractions.Messaging;

public interface IQuery<TResponse> : IRequest<Result<TResponse>>
{
}