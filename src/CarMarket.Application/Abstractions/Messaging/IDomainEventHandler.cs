﻿using CarMarket.Domain.Primitives;
using MediatR;

namespace CarMarket.Application.Abstractions.Messaging;

public interface IDomainEventHandler<TEvent> : INotificationHandler<TEvent>
    where TEvent : IDomainEvent
{
}
