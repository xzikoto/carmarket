﻿using CarMarket.Application.Abstractions.Messaging;

namespace CarMarket.Application.Items.GetAllByCategory;

public sealed record GetAllByCategoryIdRequest(int CategoryId, int itemsPerPage = 30) : IQuery<List<ItemDto>>;
