﻿namespace CarMarket.Application.Items.GetAllByCategory;

public sealed record ItemDto(Guid Id, string Name, int CategoryId, int BatteryHealth, string? Base64Image);
