﻿using AutoMapper;
using CarMarket.Application.Abstractions;
using CarMarket.Application.Abstractions.Messaging;
using CarMarket.Domain.Entities.Items;
using CarMarket.Domain.Repositories;
using CarMarket.Domain.Shared;

namespace CarMarket.Application.Items.GetAllByCategory;

internal sealed class GetAllByCategoryIdRequestHandler : IQueryHandler<GetAllByCategoryIdRequest, List<ItemDto>>
{
    private readonly IItemRepository _itemsRepository;
    private readonly IItemImagesService _itemImagesService;
    private readonly IMapper _mapper;

    public GetAllByCategoryIdRequestHandler(
        IItemRepository itemsRepository,
        IItemImagesService itemImagesService,
        IMapper mapper)
    {
        _itemsRepository = itemsRepository;
        _itemImagesService = itemImagesService;
        _mapper = mapper;
    }

    public async Task<Result<List<ItemDto>>> Handle(GetAllByCategoryIdRequest request, CancellationToken cancellationToken)
    {
        List<Item> items = await _itemsRepository.GetAllByCategoryId(request.CategoryId, cancellationToken);

        if (!items.Any())
        {
            return Enumerable.Empty<ItemDto>().ToList();
        }

        var itemsImages = await _itemImagesService.GetAllMainByCategoryIdAsync(request.CategoryId, request.itemsPerPage);

        var tasks = itemsImages.Select(async imageObject =>
        {
            using var sourceStream = imageObject.ResponseStream;
            using var memoryStream = new MemoryStream();
            await sourceStream.CopyToAsync(memoryStream);
            var base64String = Convert.ToBase64String(memoryStream.ToArray());

            //Should be fixed
            //Our main images (so called thumbnail) live
            //in separate virtual folder called "main" in the s3 bucker with lower size and quality
            //When retrieving the image, the key comes as 'main/{item.Key}/{imageName}
            return new { ItemId = imageObject.Key.Split("/").Skip(1).FirstOrDefault(), Base64String = base64String };
        });

        var idBase64Pairs = await Task.WhenAll(tasks);

        var idBase64Dictionary = idBase64Pairs.ToDictionary(pair => pair.ItemId, pair => pair.Base64String);

        var result = items.Select(item => new ItemDto(
            Id: item.Id,
            Name: item.Name,
            CategoryId: item.CategoryId,
            BatteryHealth: item.BatteryHealth,
            Base64Image: idBase64Dictionary.ContainsKey(item.Id.ToString()) ?
                         idBase64Dictionary[item.Id.ToString()] :
                         null
            )).ToList();

        return result;
    }
}
