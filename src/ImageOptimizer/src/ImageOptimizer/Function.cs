using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.Runtime.Internal.Util;
using Amazon.S3;
using Amazon.S3.Model;
using SixLabors.ImageSharp.Processing.Processors.Transforms;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace ImageOptimizer
{
    public class Function
    {
        IAmazonS3 S3Client { get; set; } = new AmazonS3Client();

        public async Task FunctionHandler(S3Event evnt, ILambdaContext context)
        {
            try
            {
                var tasks = evnt.Records.Select(async record =>
                {
                    var s3Event = record.S3;
                    if (s3Event != null)
                    {
                        var response = await this.S3Client.GetObjectMetadataAsync(s3Event.Bucket.Name, s3Event.Object.Key);
                        var isMainImage = response.Metadata["x-amz-meta-ismain"];

                        if (isMainImage == true.ToString())
                        {
                            await ProcessMainImage(s3Event);
                        }

                        var isImageResized = response.Metadata["x-amz-meta-amz-meta-resized"];
                        if (isImageResized == false.ToString())
                        {
                            await LoadAndProcessImage(s3Event.Bucket.Name, s3Event.Object.Key, 1600, 1200);
                        }
                    }
                });

                await Task.WhenAll(tasks);
            }
            catch (Exception e)
            {
                context.Logger.LogError("Error processing S3 events: " + e.Message);
                context.Logger.LogError(e.StackTrace);
            }
        }

        private async Task ProcessMainImage(S3Event.S3Entity s3Event)
        {
            string main_folder_prefix = "main";
            using (var image = await LoadAndProcessImage(s3Event.Bucket.Name, s3Event.Object.Key))
            {
                await SaveImage(s3Event.Bucket.Name, main_folder_prefix + "/" +s3Event.Object.Key, image);
            }
        }

        private async Task<Image> LoadAndProcessImage(
            string bucketName, 
            string objectKey, 
            int width = 800, 
            int height = 600)
        {
            using (var itemStream = await S3Client.GetObjectStreamAsync(bucketName, objectKey, new Dictionary<string, object>()))
            {
                var image = await Image.LoadAsync(itemStream);
                image.Mutate(x => x.Resize(width, height, LanczosResampler.Lanczos5));
                return image;
            }
        }

        private async Task SaveImage(string bucketName, string objectKey, Image image)
        {
            using var outStream = new MemoryStream();
            await image.SaveAsJpegAsync(outStream);

            var putObjectData = new PutObjectRequest
            {
                BucketName = bucketName,
                Key = objectKey,
                InputStream = outStream,
            };

            await S3Client.PutObjectAsync(putObjectData);
        }
    }
}