﻿using Amazon.S3;
using Amazon.S3.Model;
using CarMarket.Application.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace CarMarket.Infrastructure.Services.AwsServices;

internal sealed class ItemImagesService : IItemImagesService
{
    private readonly AwsOptions _awsOptions;
    private readonly IAmazonS3 _amazonS3;

    private const int MAX_CONCURRENT_REQUESTS = 20;
    private const string MAIN_IMAGE_FOLDER_PREFIX = "main";
    public ItemImagesService(IOptions<AwsOptions> awsOptions, IAmazonS3 amazonS3)
    {
        _awsOptions = awsOptions.Value;
        _amazonS3 = amazonS3;
    }

    public async Task<DeleteObjectResponse> DeleteImageAsync(Guid itemId, string imageName)
    {
        var deleteObjectData = new DeleteObjectRequest
        {
            BucketName = _awsOptions.BucketName,
            Key = itemId.ToString() + "/" + imageName
        };

        return await _amazonS3.DeleteObjectAsync(deleteObjectData);
    }

    public async Task<GetObjectResponse?> GetImageAsync(Guid itemId, string imageName)
    {
        try
        {
            var getObjectData = new GetObjectRequest
            {
                BucketName = _awsOptions.BucketName,
                Key = itemId.ToString() + "/" + imageName
            };

            return await _amazonS3.GetObjectAsync(getObjectData);
        }
        catch (AmazonS3Exception ex) when (ex.Message is "The specified key does not exist. Key")
        {
            return null;
        }
    }

    public async Task<List<GetObjectResponse>?> GetAllItemImagesAsync(Guid itemId, int itemsPerPage)
    {
        try
        {
            var listObjectKeys = await _amazonS3.ListObjectsAsync(_awsOptions.BucketName, itemId.ToString());

            var semaphore = new SemaphoreSlim(MAX_CONCURRENT_REQUESTS);

            var getObjectTasks = new List<Task<GetObjectResponse>>();

            foreach (var entry in listObjectKeys.S3Objects)
            {
                await semaphore.WaitAsync();

                var s3GetObject = _amazonS3.GetObjectAsync(new GetObjectRequest
                {
                    BucketName = _awsOptions.BucketName,
                    Key = entry.Key
                });

                await s3GetObject.ContinueWith(t =>
                {
                    semaphore.Release();
                });

                getObjectTasks.Add(s3GetObject);
            }

            var getObjectResponses = await Task.WhenAll(getObjectTasks);

            return getObjectResponses.ToList();
        }
        catch (AmazonS3Exception ex) when (ex.Message is "The specified key does not exist.")
        {
            return null;
        }
    }
    public async Task<List<GetObjectResponse>?> GetAllMainByCategoryIdAsync(int categoryId, int itemsPerPage)
    {
        try
        {
            var listObjectKeys = await _amazonS3.ListObjectsAsync(_awsOptions.BucketName, MAIN_IMAGE_FOLDER_PREFIX);

            var semaphore = new SemaphoreSlim(MAX_CONCURRENT_REQUESTS);

            var getObjectTasks = new List<Task<GetObjectResponse>>();

            foreach (var entry in listObjectKeys.S3Objects)
            {
                await semaphore.WaitAsync();

                var s3GetObject = _amazonS3.GetObjectAsync(new GetObjectRequest
                {
                    BucketName = _awsOptions.BucketName,
                    Key = entry.Key
                });

                await s3GetObject.ContinueWith(t =>
                {
                    semaphore.Release();
                });

                getObjectTasks.Add(s3GetObject);
            }

            var getObjectResponses = await Task.WhenAll(getObjectTasks);

            return getObjectResponses.ToList();
        }
        catch (AmazonS3Exception ex) when (ex.Message is "The specified key does not exist.")
        {
            return null;
        }
    }

    public async Task<PutObjectResponse> UploadImageAsync(Guid itemId, IFormFile file, bool isMain)
    {
        string directoryName = itemId.ToString();
        string objectKey = $"{directoryName}/{file.FileName}";

        var putObjectData = new PutObjectRequest
        {
            BucketName = _awsOptions.BucketName,
            Key = objectKey,
            ContentType = file.ContentType,
            InputStream = file.OpenReadStream(),
            Metadata =
            {
                ["x-amz-meta-originalname"] = file.Name,
                ["x-amz-meta-extension"] = Path.GetExtension(file.Name),
                ["x-amz-meta-ismain"] = isMain.ToString()
            },
        };

        return await _amazonS3.PutObjectAsync(putObjectData);
    }
}
