﻿namespace CarMarket.Infrastructure.Services.AwsServices
{
    public class AwsOptions
    {
        public string BucketName { get; init; }

        public string AccessKey { get; init; }
        
        public string SecretKey { get; init; }

        public string Profile { get; init; }
        
        public string Region { get; init; }
    }
}
