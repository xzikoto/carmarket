﻿using CarMarket.Domain.Enums;
using Microsoft.AspNetCore.Authorization;

namespace CarMarket.Infrastructure.Authentication;
public sealed class HasPermissionAttribute : AuthorizeAttribute
{
    public HasPermissionAttribute(Permission permission) : base(policy: permission.ToString())
    {

    }
}
