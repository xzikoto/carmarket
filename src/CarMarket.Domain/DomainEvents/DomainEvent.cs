﻿using CarMarket.Domain.Primitives;

namespace CarMarket.Domain.DomainEvents;

public abstract record DomainEvent(Guid Id) : IDomainEvent;
