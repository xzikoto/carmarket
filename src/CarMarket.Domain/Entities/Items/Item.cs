﻿using CarMarket.Domain.Primitives;

namespace CarMarket.Domain.Entities.Items
{
    public class Item : AggregateRoot, IAuditableEntity
    {
        public Item(Guid id) : base(id) 
        {
            
        }
        public string Name { get; set; }

        public int CategoryId { get; set; }

        public int BatteryHealth { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime? ModifiedOnUtc { get; set; }
    }
}
