﻿
namespace CarMarket.Domain.Entities.Localizations
{
    public class Language
    {
        public int Id { get; init; }
        public string Name { get; init; } = string.Empty;
        public string Code { get; set; } = string.Empty;
    }
}
