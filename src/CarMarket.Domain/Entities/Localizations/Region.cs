﻿
namespace CarMarket.Domain.Entities.Localizations
{
    //By Region we mean continent - Europe, Asia etc.
    public class Region
    {
        public int Id { get; init; }
        public string Name { get; init; } = string.Empty;
        public string Code { get; set; } = string.Empty;
    }
}
