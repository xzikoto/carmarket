﻿using CarMarket.Domain.Primitives;

namespace CarMarket.Domain.Entities.Localizations
{
    public class CategoryLocalization : IAuditableEntity
    {
        public string Name { get; set; }

        public Category Category { get; set; }
        public int CategoryId { get; set; }

        public Language Language { get; set; }
        public int LanguageId { get; set; }

        public Region Region { get; set; }
        public int RegionId { get; set; }

        public DateTime CreatedOnUtc { get; set; }
        public DateTime? ModifiedOnUtc { get; set; }
    }
}
