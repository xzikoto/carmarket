﻿
namespace CarMarket.Domain.Entities
{
    using CarMarket.Domain.Primitives;
    using System;

    public class Category : IAuditableEntity
    {
        public int Id { get; set; }
        public required string Name { get; set; }

        public string IconClass { get; set; }

        public DateTime CreatedOnUtc { get; set; }
        public DateTime? ModifiedOnUtc { get; set; }
    }
}
