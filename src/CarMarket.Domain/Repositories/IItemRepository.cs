﻿using CarMarket.Domain.Entities.Items;

namespace CarMarket.Domain.Repositories;

public interface IItemRepository
{
    Task<List<Item>> GetAllByCategoryId(int categoryId, CancellationToken cancellationToken = default);
}
