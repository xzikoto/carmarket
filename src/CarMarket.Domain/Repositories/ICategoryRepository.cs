﻿using CarMarket.Domain.Entities;

namespace CarMarket.Domain.Repositories;

public interface ICategoryRepository
{
    Task<List<Category>> GetAll(int languageId, CancellationToken cancellationToken = default);
}
