﻿namespace CarMarket.Domain.Enums
{
    public enum Categories
    {
        Car = 1,
        Bus = 2,
        Truck = 3,
        Bicycle = 4,
        Scooter = 5,
    }
}
