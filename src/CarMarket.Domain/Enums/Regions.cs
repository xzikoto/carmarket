﻿namespace CarMarket.Domain.Enums
{
    public enum Regions
    {
        Europe = 1,
        Asia = 2,
        Africa = 3,
        NorthAmerica = 4,
        SouthAmerica = 5,
        Antarctica = 6,
        Australia = 7
    }
}
