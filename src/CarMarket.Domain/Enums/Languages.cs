﻿namespace CarMarket.Domain.Enums
{
    public enum Languages
    {
        English = 1,
        Bulgarian = 2
    }
}
