﻿using CarMarket.Domain.Entities.Items;
using Newtonsoft.Json;

namespace CarMarket.Persistence.Seeder
{
    public static class ItemsSeeder
    {
        private static string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Seeder\items.json");

        public static List<Item> Items { get; set; }

        public static void LoadPredefinedValuesFromJson()
        {
            var config = JsonConvert.DeserializeObject<ConfigurationItems>(File.ReadAllText(filePath));

            Items = new List<Item>();
            LoadItems(config);
        }

        private static void LoadItems(ConfigurationItems? config)
        {
            foreach (var item in config.Items)
            {
                var mappedItem = new Item(Guid.Parse(item.Id))
                {
                    CategoryId = item.CategoryId,
                    Name = item.Name,
                    BatteryHealth = item.BatteryHealth
                };

                Items.Add(mappedItem);
            }
        }
    }

    public class ConfigurationItems
    {
        public List<ConfigurationItem> Items { get; set; }
    }

    public class ConfigurationItem
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public int CategoryId { get; set; }

        public int BatteryHealth { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime? ModifiedOnUtc { get; set; }
    }
}
