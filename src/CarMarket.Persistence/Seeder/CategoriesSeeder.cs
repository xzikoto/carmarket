﻿using CarMarket.Domain.Entities;
using CarMarket.Domain.Entities.Localizations;
using Newtonsoft.Json;
using System.Linq;

namespace CarMarket.Persistence.Seeder
{
    //TODO: FIX SEEDER!
    //Should be fixed !!! - for now works for testing
    //Should be thread safe when app running in several instances
    //Should be extracted to in a service to use dbcontext, because now it overwrites every time all the category localization
    public static class CategoriesSeeder
    {
        private static string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Seeder\categories.json");

        public static List<CategoryLocalization> CategoriesLocalizations { get; set; }
        public static Dictionary<int, string> CategoryIconClassesDictionary { get; set; }

        public static void LoadPredefinedValuesFromJson()
        {
            var config = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(filePath));

            CategoriesLocalizations = new List<CategoryLocalization>();
            CategoryIconClassesDictionary = new Dictionary<int, string>();
            LoadCategoriesLocalizations(config);
        }

        private static void LoadCategoriesLocalizations(Configuration? config)
        {
            foreach (var category in config.Categories)
            {
                CategoriesLocalizations.AddRange(
                    category.Localizations.Select(l => new CategoryLocalization
                    {
                        CategoryId = category.Id,
                        LanguageId = l.LanguageId,
                        RegionId = l.RegionId,
                        Name = l.Name,
                        CreatedOnUtc = DateTime.UtcNow,
                    }).ToList());

                if (!CategoryIconClassesDictionary.ContainsKey(category.Id))
                {
                    CategoryIconClassesDictionary.Add(category.Id, category.IconClass);
                }
            }
        }
    }

    public class Configuration
    {
        public List<ConfigCategory> Categories { get; set; }
    }

    public class ConfigCategory
    {
        public int Id { get; set; }
        public string IconClass { get; set; }
        public List<ConfigCategoryLocalization> Localizations { get; set; }
    }

    public class ConfigCategoryLocalization
    {
        public int LanguageId { get; set; }
        public int RegionId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}
