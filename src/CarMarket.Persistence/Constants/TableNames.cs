﻿namespace CarMarket.Persistence.Constants;

internal static class TableNames
{
    internal const string Members = nameof(Members);

    internal const string Invitations = nameof(Invitations);

    internal const string Attendees = nameof(Attendees);

    internal const string OutboxMessages = nameof(OutboxMessages);

    internal const string OutboxMessageConsumers = nameof(OutboxMessageConsumers);

    internal const string Roles = nameof(Roles);

    internal const string Permissions = nameof(Permissions);

    internal const string Languages = nameof(Languages);

    internal const string Regions = nameof(Regions);
    
    internal const string CategoryLocalizations = nameof(CategoryLocalizations);

    internal const string Categories = nameof(Categories);

    internal const string Items = nameof(Items);
}
