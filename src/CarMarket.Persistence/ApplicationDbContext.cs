﻿using CarMarket.Domain.Entities;
using CarMarket.Domain.Entities.Items;
using CarMarket.Domain.Entities.Localizations;
using CarMarket.Domain.Enums;
using CarMarket.Persistence.Seeder;
using Microsoft.EntityFrameworkCore;

namespace CarMarket.Persistence;

public sealed class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(AssemblyReference.Assembly);

        SeedInitialData(modelBuilder);
    }

    private void SeedInitialData(ModelBuilder modelBuilder)
    {
        CategoriesSeeder.LoadPredefinedValuesFromJson();
        modelBuilder.Entity<CategoryLocalization>().HasData(CategoriesSeeder.CategoriesLocalizations);

        IEnumerable<Category> permissions = Enum
            .GetValues<Categories>()
            .Select(p => new Category
            {
                Id = (int)p,
                Name = p.ToString(),
                IconClass = CategoriesSeeder.CategoryIconClassesDictionary.ContainsKey((int)p) ? CategoriesSeeder.CategoryIconClassesDictionary[(int)p] : string.Empty
            });
        modelBuilder.Entity<Category>().HasData(permissions);
       
        ItemsSeeder.LoadPredefinedValuesFromJson();
        modelBuilder.Entity<Item>().HasData(ItemsSeeder.Items);
    }
}