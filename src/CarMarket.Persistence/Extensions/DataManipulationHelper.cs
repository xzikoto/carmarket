﻿namespace CarMarket.Persistence.Extensions
{
    using System.Text.RegularExpressions;
    public static class DataManipulationHelper
    {
        public const string EMPTY_SPACE = " ";
        public static string SplitByCamelCase(string data, string separator = EMPTY_SPACE) 
            => string.Join(separator, Regex.Split(data, @"(?<!^)(?=[A-Z])"));
    }
}
