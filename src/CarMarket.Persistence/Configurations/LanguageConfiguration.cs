﻿namespace CarMarket.Persistence.Configurations
{
    using CarMarket.Domain.Entities.Localizations;
    using CarMarket.Domain.Enums;
    using CarMarket.Persistence.Constants;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    
    internal class LanguageConfiguration : IEntityTypeConfiguration<Language>
    {
        public void Configure(EntityTypeBuilder<Language> builder)
        {
            builder.ToTable(TableNames.Languages);

            builder.HasKey(p => p.Id);

            IEnumerable<Language> permissions = Enum
                .GetValues<Languages>()
                .Select(p => new Language
                {
                    Id = (int)p,
                    Name = p.ToString()
                });

            builder.HasData(permissions);
        }
    }
}
