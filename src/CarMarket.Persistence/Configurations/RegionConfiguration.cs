﻿namespace CarMarket.Persistence.Configurations
{
    using CarMarket.Domain.Entities.Localizations;
    using CarMarket.Domain.Enums;
    using CarMarket.Persistence.Constants;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    
    internal class RegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.ToTable(TableNames.Regions);

            builder.HasKey(p => p.Id);

            IEnumerable<Region> permissions = Enum
                .GetValues<Regions>()
                .Select(p => new Region
                {
                    Id = (int)p,
                    Name = p.ToString()
                });

            builder.HasData(permissions);
        }
    }
}
