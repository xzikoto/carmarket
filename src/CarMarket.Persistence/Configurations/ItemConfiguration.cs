﻿using CarMarket.Domain.Entities.Items;
using CarMarket.Persistence.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CarMarket.Persistence.Configurations;

internal sealed class ItemConfiguration : IEntityTypeConfiguration<Item>
{
    public void Configure(EntityTypeBuilder<Item> builder)
    {
        builder.ToTable(TableNames.Items);

        builder.HasKey(x => x.Id);

        builder.Property(x => x.Name);

        builder.Property(x => x.CategoryId);

        builder.Property(x => x.BatteryHealth);

        builder.Property(x => x.CreatedOnUtc);

        builder.Property(x => x.ModifiedOnUtc);
    }
}
