﻿namespace CarMarket.Persistence.Configurations;

using CarMarket.Domain.Entities.Localizations;
using CarMarket.Persistence.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

internal class CategoryLocalizationConfiguration : IEntityTypeConfiguration<CategoryLocalization>
{
    public void Configure(EntityTypeBuilder<CategoryLocalization> builder)
    {
        builder.ToTable(TableNames.CategoryLocalizations);

        builder.HasKey(x => new { x.CategoryId, x.LanguageId, x.RegionId });

        builder.HasOne(cl => cl.Category)
               .WithMany()
               .HasForeignKey(cl => cl.CategoryId);

        builder.HasOne(cl => cl.Language)
               .WithMany()
               .HasForeignKey(cl => cl.LanguageId);

        builder.HasOne(cl => cl.Region) 
               .WithMany()
               .HasForeignKey(cl => cl.RegionId);

        builder.Property(cl => cl.Name);

        builder.Property(cl => cl.CreatedOnUtc)
               .IsRequired();

        builder.Property(cl => cl.ModifiedOnUtc);

        builder.HasIndex(x => new { x.CategoryId, x.LanguageId, x.RegionId })
               .IsUnique();
    }
}