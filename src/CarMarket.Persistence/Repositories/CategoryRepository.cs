﻿using CarMarket.Domain.Entities;
using CarMarket.Domain.Entities.Localizations;
using CarMarket.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarMarket.Persistence.Repositories;

public sealed class CategoryRepository : ICategoryRepository
{
    private readonly ApplicationDbContext _dbContext;

    public CategoryRepository(ApplicationDbContext dbContext) =>
        _dbContext = dbContext;

    public async Task<List<Category>> GetAll(int languageId, CancellationToken cancellationToken = default)
    {
        var categories = _dbContext
            .Set<CategoryLocalization>()
            .Include(x => x.Category)
            .Where(cl => cl.LanguageId == languageId)
            .Select(cl => new Category 
            { 
                Id = cl.CategoryId,
                Name = cl.Name,
                IconClass = cl.Category.IconClass,
                CreatedOnUtc = cl.CreatedOnUtc,
                ModifiedOnUtc = cl.ModifiedOnUtc,
            });

        return await categories.ToListAsync(cancellationToken);
    }
}
