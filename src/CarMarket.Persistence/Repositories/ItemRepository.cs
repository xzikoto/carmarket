﻿using CarMarket.Domain.Entities.Items;
using CarMarket.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CarMarket.Persistence.Repositories;

public sealed class ItemRepository : IItemRepository
{
    private readonly ApplicationDbContext _dbContext;

    public ItemRepository(ApplicationDbContext dbContext) =>
        _dbContext = dbContext;

    public Task<List<Item>> GetAllByCategoryId(int categoryId, CancellationToken cancellationToken = default)
    {
        return _dbContext
            .Set<Item>()
            .Where(i => i.CategoryId == categoryId)
            .ToListAsync(cancellationToken);
    }
}
