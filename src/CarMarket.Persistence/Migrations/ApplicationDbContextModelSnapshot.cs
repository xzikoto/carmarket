﻿// <auto-generated />
using System;
using CarMarket.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace CarMarket.Persistence.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.12")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("CarMarket.Domain.Entities.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<DateTime>("CreatedOnUtc")
                        .HasColumnType("datetime2");

                    b.Property<string>("IconClass")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("ModifiedOnUtc")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Categories", (string)null);

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            IconClass = "bi bi-car-front-fill",
                            Name = "Car"
                        },
                        new
                        {
                            Id = 2,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            IconClass = "bi bi-bus-front-fill",
                            Name = "Bus"
                        },
                        new
                        {
                            Id = 3,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            IconClass = "bi bi-truck-front-fill",
                            Name = "Truck"
                        },
                        new
                        {
                            Id = 4,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            IconClass = "bi bi-bicycle",
                            Name = "Bicycle"
                        },
                        new
                        {
                            Id = 5,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            IconClass = "bi bi-scooter",
                            Name = "Scooter"
                        });
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Items.Item", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("BatteryHealth")
                        .HasColumnType("int");

                    b.Property<int>("CategoryId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOnUtc")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("ModifiedOnUtc")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Items", (string)null);

                    b.HasData(
                        new
                        {
                            Id = new Guid("71488c41-ff77-44ad-8436-333b50e41a74"),
                            BatteryHealth = 93,
                            CategoryId = 1,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Reno Zoe"
                        },
                        new
                        {
                            Id = new Guid("52e3219e-aefe-4986-b3d6-5c69cb840384"),
                            BatteryHealth = 73,
                            CategoryId = 1,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "BMW i3"
                        },
                        new
                        {
                            Id = new Guid("a10788fb-eff2-4b77-a854-64105be962bd"),
                            BatteryHealth = 53,
                            CategoryId = 1,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Tesla p3"
                        },
                        new
                        {
                            Id = new Guid("e216b9ba-e367-4bb4-9517-1f69b985516c"),
                            BatteryHealth = 43,
                            CategoryId = 2,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "AB Volvo 7900"
                        },
                        new
                        {
                            Id = new Guid("40e5743d-4596-473c-9a22-1535a0e44546"),
                            BatteryHealth = 23,
                            CategoryId = 2,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Mercedes-Benz eCitaro G"
                        },
                        new
                        {
                            Id = new Guid("5a9bd5d3-e756-4fe8-bfa7-104890a3f409"),
                            BatteryHealth = 13,
                            CategoryId = 2,
                            CreatedOnUtc = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Name = "Solaris Urbino 18 Electric"
                        });
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Localizations.CategoryLocalization", b =>
                {
                    b.Property<int>("CategoryId")
                        .HasColumnType("int");

                    b.Property<int>("LanguageId")
                        .HasColumnType("int");

                    b.Property<int>("RegionId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedOnUtc")
                        .HasColumnType("datetime2");

                    b.Property<DateTime?>("ModifiedOnUtc")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("CategoryId", "LanguageId", "RegionId");

                    b.HasIndex("LanguageId");

                    b.HasIndex("RegionId");

                    b.HasIndex("CategoryId", "LanguageId", "RegionId")
                        .IsUnique();

                    b.ToTable("CategoryLocalizations", (string)null);

                    b.HasData(
                        new
                        {
                            CategoryId = 1,
                            LanguageId = 1,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2392),
                            Name = "Car"
                        },
                        new
                        {
                            CategoryId = 1,
                            LanguageId = 2,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2396),
                            Name = "Кола"
                        },
                        new
                        {
                            CategoryId = 3,
                            LanguageId = 1,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2417),
                            Name = "Truck"
                        },
                        new
                        {
                            CategoryId = 3,
                            LanguageId = 2,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2418),
                            Name = "Камион"
                        },
                        new
                        {
                            CategoryId = 4,
                            LanguageId = 1,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2422),
                            Name = "Bicycle"
                        },
                        new
                        {
                            CategoryId = 4,
                            LanguageId = 2,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2423),
                            Name = "Колело"
                        },
                        new
                        {
                            CategoryId = 2,
                            LanguageId = 1,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2427),
                            Name = "Bus"
                        },
                        new
                        {
                            CategoryId = 2,
                            LanguageId = 2,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2427),
                            Name = "Бус"
                        },
                        new
                        {
                            CategoryId = 5,
                            LanguageId = 1,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2471),
                            Name = "Scooter"
                        },
                        new
                        {
                            CategoryId = 5,
                            LanguageId = 2,
                            RegionId = 1,
                            CreatedOnUtc = new DateTime(2023, 10, 31, 16, 32, 24, 170, DateTimeKind.Utc).AddTicks(2472),
                            Name = "Тротинетка"
                        });
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Localizations.Language", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Languages", (string)null);

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Code = "",
                            Name = "English"
                        },
                        new
                        {
                            Id = 2,
                            Code = "",
                            Name = "Bulgarian"
                        });
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Localizations.Region", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Code")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Regions", (string)null);

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Code = "",
                            Name = "Europe"
                        },
                        new
                        {
                            Id = 2,
                            Code = "",
                            Name = "Asia"
                        },
                        new
                        {
                            Id = 3,
                            Code = "",
                            Name = "Africa"
                        },
                        new
                        {
                            Id = 4,
                            Code = "",
                            Name = "NorthAmerica"
                        },
                        new
                        {
                            Id = 5,
                            Code = "",
                            Name = "SouthAmerica"
                        },
                        new
                        {
                            Id = 6,
                            Code = "",
                            Name = "Antarctica"
                        },
                        new
                        {
                            Id = 7,
                            Code = "",
                            Name = "Australia"
                        });
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Member", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("CreatedOnUtc")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<DateTime?>("ModifiedOnUtc")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique();

                    b.ToTable("Members", (string)null);
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Permission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Permissions", (string)null);

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "ReadMember"
                        },
                        new
                        {
                            Id = 2,
                            Name = "UpdateMember"
                        },
                        new
                        {
                            Id = 3,
                            Name = "ReadMemberV2"
                        });
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Roles", (string)null);

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Registered"
                        });
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.RolePermission", b =>
                {
                    b.Property<int>("PermissionId")
                        .HasColumnType("int");

                    b.Property<int>("RoleId")
                        .HasColumnType("int");

                    b.HasKey("PermissionId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("RolePermission");
                });

            modelBuilder.Entity("MemberRole", b =>
                {
                    b.Property<Guid>("MembersId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("RolesId")
                        .HasColumnType("int");

                    b.HasKey("MembersId", "RolesId");

                    b.HasIndex("RolesId");

                    b.ToTable("MemberRole");
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.Localizations.CategoryLocalization", b =>
                {
                    b.HasOne("CarMarket.Domain.Entities.Category", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CarMarket.Domain.Entities.Localizations.Language", "Language")
                        .WithMany()
                        .HasForeignKey("LanguageId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CarMarket.Domain.Entities.Localizations.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Category");

                    b.Navigation("Language");

                    b.Navigation("Region");
                });

            modelBuilder.Entity("CarMarket.Domain.Entities.RolePermission", b =>
                {
                    b.HasOne("CarMarket.Domain.Entities.Permission", null)
                        .WithMany()
                        .HasForeignKey("PermissionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CarMarket.Domain.Entities.Role", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MemberRole", b =>
                {
                    b.HasOne("CarMarket.Domain.Entities.Member", null)
                        .WithMany()
                        .HasForeignKey("MembersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CarMarket.Domain.Entities.Role", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
