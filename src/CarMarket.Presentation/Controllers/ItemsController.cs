﻿using AutoMapper;
using CarMarket.Application.Items.GetAllByCategory;
using CarMarket.Domain.Shared;
using CarMarket.Presentation.Abstractions;
using CarMarket.Presentation.Contracts.Items;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CarMarket.Presentation.Controllers;

[Route("api/[controller]")]
public sealed class ItemsController : ApiController
{
    public ItemsController(
        ISender sender, 
        IMapper mapper) : base(sender, mapper)
    {
    }

    [HttpGet]
    [Route("{categoryId:int}")]
    public async Task<IActionResult> GetAllByCategoryId([FromRoute] int categoryId, CancellationToken cancellationToken)
    {
        var query = new GetAllByCategoryIdRequest(categoryId);

        Result<List<ItemDto>> response = await Sender.Send(query, cancellationToken);
        
        return response.IsSuccess ? Ok(Mapper!.Map<List<ItemsResponse>>(response.Value)) : NotFound(response.Error);
    }
}
