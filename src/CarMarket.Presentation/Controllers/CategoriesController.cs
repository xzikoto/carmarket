﻿using CarMarket.Domain.Entities;
using CarMarket.Domain.Repositories;
using CarMarket.Domain.Shared;
using CarMarket.Presentation.Abstractions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CarMarket.Presentation.Controllers
{
    [Route("api/[controller]")]
    public sealed class CategoriesController : ApiController
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoriesController(
            ISender sender,
            ICategoryRepository categoryRepository) : base(sender)
        {
            _categoryRepository = categoryRepository;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<IActionResult> GetAll([FromRoute] int id, CancellationToken cancellationToken)
        {
            Result<List<Category>> response = await _categoryRepository.GetAll(id, cancellationToken);

            return response.IsSuccess ? Ok(response.Value) : NotFound(response.Error);
        }
    }
}
