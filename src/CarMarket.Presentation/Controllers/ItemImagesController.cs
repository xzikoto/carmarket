﻿using System.Net;
using CarMarket.Application.Abstractions;
using CarMarket.Presentation.Abstractions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarMarket.Presentation.Controllers
{
    [Route("api/item-images")]
    public class ItemImagesController : ApiController
    {
        private readonly IItemImagesService _itemImagesService;

        public ItemImagesController(ISender sender, IItemImagesService itemImagesService) : base(sender)
        {
            _itemImagesService = itemImagesService;
        }

        [HttpGet("{itemId:Guid}/{imageName}/image")]
        public async Task<IActionResult> Get([FromRoute] Guid itemId, [FromRoute] string imageName)
        {
            var response = await _itemImagesService.GetImageAsync(itemId, imageName);

            if (response is null)
            {
                return NotFound();
            }

            return File(response.ResponseStream, response.Headers.ContentType);
        }

        [HttpGet("{itemId:Guid}/images")]
        public async Task<IActionResult> GetAll([FromRoute] Guid itemId, [FromQuery] int itemsPerPage)
        {
            var response = await _itemImagesService.GetAllItemImagesAsync(itemId, itemsPerPage);

            if (response is null)
            {
                return NotFound();
            }

            return Ok(response);
        }

        [HttpDelete("{itemId:Guid}/{imageName}/image")]
        public async Task<IActionResult> Delete([FromRoute] Guid itemId,[FromRoute] string imageName)
        {
            var response = await _itemImagesService.DeleteImageAsync(itemId, imageName);

            return response.HttpStatusCode switch
            {
                HttpStatusCode.OK => Ok(),
                HttpStatusCode.NotFound => NotFound(),
                _ => BadRequest()
            };
        }

        [HttpPost("{itemId:Guid}/image")]
        public async Task<IActionResult> Upload(
            [FromRoute] Guid itemId,
            [FromForm(Name = "Data")] IFormFile file,
            [FromQuery] bool isMain = false)
        {
            var response = await _itemImagesService.UploadImageAsync(itemId, file, isMain);

            if (response.HttpStatusCode == HttpStatusCode.OK)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
