﻿namespace CarMarket.Presentation.Contracts.Items;

public sealed record ItemsResponse(Guid Id, string Name, int CategoryId, int BatteryHealth, string? Base64Image);
