﻿using AutoMapper;
using CarMarket.Application.Items.GetAllByCategory;
using CarMarket.Presentation.Contracts.Items;

namespace CarMarket.Presentation.Mappings.Items;

public class ItemsDtoToContractMapper : Profile
{
    public ItemsDtoToContractMapper()
    {
        CreateMap<ItemDto, ItemsResponse>();
    }
}