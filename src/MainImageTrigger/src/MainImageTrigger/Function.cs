using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Model;
using SixLabors.ImageSharp.Processing.Processors.Transforms;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace MainImageTrigger
{
    public class Function
    {
        private const string MAIN_IMAGES_FOLDER = "main";

        IAmazonS3 S3Client { get; set; } = new AmazonS3Client();

        public async Task FunctionHandler(S3Event evnt, ILambdaContext context)
        {
            var eventRecords = evnt.Records ?? new List<S3Event.S3EventNotificationRecord>();
            foreach (var record in eventRecords)
            {
                var s3Event = record.S3;
                if (s3Event == null)
                {
                    context.Logger.LogError("s3Event is null");

                    continue;
                }

                try
                {
                    var response = await this.S3Client.GetObjectMetadataAsync(s3Event.Bucket.Name, s3Event.Object.Key);

                    if (response.Metadata["x-amz-meta-ismain"] == false.ToString())
                    {
                        context.Logger.LogInformation($"Main image for this item: {s3Event.Object.Key} is already uploaded");
                        continue;
                    }

                    await using var itemStream = await this.S3Client.GetObjectStreamAsync(s3Event.Bucket.Name, s3Event.Object.Key,
                        new Dictionary<string, object>());

                    using var outStream = new MemoryStream();
                    using (var image = await Image.LoadAsync(itemStream))
                    {
                        image.Mutate(x => x.Resize(600, 400, LanczosResampler.Lanczos8));

                        await image.SaveAsJpegAsync(outStream);
                    }

                    var putObjectData = new PutObjectRequest
                    {
                        BucketName = s3Event.Bucket.Name,
                        Key = MAIN_IMAGES_FOLDER + "/" + s3Event.Object.Key,
                        Metadata =
                        {
                            ["x-amz-meta-originalname"] = response.Metadata["x-amz-meta-originalname"],
                            ["x-amz-meta-extension"] = response.Metadata["x-amz-meta-extension"],
                            ["x-amz-meta-resized"] = true.ToString(),
                            ["x-amz-meta-ismain"] = true.ToString()
                        },
                        ContentType = response.Headers.ContentType,
                        InputStream = outStream
                    };

                    await S3Client.PutObjectAsync(putObjectData);


                }
                catch (Exception e)
                {
                    context.Logger.LogError($"Error getting object {s3Event.Object.Key} from bucket {s3Event.Bucket.Name}. Make sure they exist and your bucket is in the same region as this function.");
                    context.Logger.LogError(e.Message);
                    context.Logger.LogError(e.StackTrace);
                    throw;
                }
            }
        }
    }
}